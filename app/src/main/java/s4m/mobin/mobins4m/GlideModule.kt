package s4m.mobin.mobins4m

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * Created by MohammedMobinMunir on 4/17/2018.
 */
@GlideModule
class GlideModule : AppGlideModule()