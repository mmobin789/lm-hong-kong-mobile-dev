package s4m.mobin.mobins4m.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.adapter_ui.*
import s4m.mobin.mobins4m.GlideApp
import s4m.mobin.mobins4m.R
import s4m.mobin.mobins4m.models.Delivery

/**
 * Created by MohammedMobinMunir on 4/17/2018.
 */
class DeliveryOfflineAdapter(private val onItemClickListener: OnItemClickListener, val list: List<Delivery>) : RecyclerView.Adapter<DeliveryOfflineAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_ui, parent, false))
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(list[position])

    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        init {
            containerView.setOnClickListener {

                onItemClickListener.onItemClick(list[adapterPosition])

            }
        }

        fun bind(delivery: Delivery?) {
            if (delivery != null) {
                tv.text = if (delivery.description.isNullOrBlank())
                    containerView.context.getString(R.string.no_desc)
                else delivery.description
                GlideApp.with(containerView).load(delivery.imageUrl).placeholder(R.drawable.ic_launcher_background).into(iv)
            }
        }


    }


    interface OnItemClickListener {
        fun onItemClick(delivery: Delivery?)
    }


}