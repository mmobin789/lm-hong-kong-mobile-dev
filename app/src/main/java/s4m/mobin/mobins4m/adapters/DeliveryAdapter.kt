package s4m.mobin.mobins4m.adapters

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.adapter_footer.*
import kotlinx.android.synthetic.main.adapter_ui.*
import s4m.mobin.mobins4m.GlideApp
import s4m.mobin.mobins4m.R
import s4m.mobin.mobins4m.models.Delivery
import s4m.mobin.mobins4m.pagination.State

/**
 * Created by MohammedMobinMunir on 4/17/2018.
 */
class DeliveryAdapter(private val onItemClickListener: OnItemClickListener, private val retry: () -> Unit) : PagedListAdapter<Delivery, DeliveryAdapter.ViewHolder>(deliveryDiffCallback) {


    private val data = 1
    private val footer = 2
    private var state = State.LOADING
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(if (viewType == data) R.layout.adapter_ui else R.layout.adapter_footer, parent, false))
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasFooter()) 1 else 0
    }

    private fun hasFooter(): Boolean {
        return super.getItemCount() != 0 && (state == State.LOADING || state == State.ERROR)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position < super.getItemCount()) data else footer
    }

    fun setState(state: State) {
        this.state = state
        notifyItemChanged(super.getItemCount())
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (getItemViewType(position) == data) {
            holder.bind(getItem(position))
        } else {
            holder.bind(state)
        }
    }


    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        init {
            containerView.setOnClickListener {
                if (itemViewType == data)
                    onItemClickListener.onItemClick(getItem(adapterPosition))
                else retry()
            }
        }

        fun bind(delivery: Delivery?) {
            if (delivery != null) {
                tv.text = if (delivery.description.isNullOrBlank())
                    containerView.context.getString(R.string.no_desc)
                else delivery.description
                GlideApp.with(containerView).load(delivery.imageUrl).placeholder(R.drawable.ic_launcher_background).into(iv)
            }
        }

        fun bind(status: State) {
            loader.visibility = if (status == State.LOADING) View.VISIBLE else View.INVISIBLE
            errorTV.visibility = if (status == State.ERROR) View.VISIBLE else View.INVISIBLE
        }
    }

    companion object {
        val deliveryDiffCallback = object : DiffUtil.ItemCallback<Delivery>() {
            override fun areContentsTheSame(oldItem: Delivery, newItem: Delivery): Boolean {
                return oldItem.description == newItem.description
            }

            override fun areItemsTheSame(oldItem: Delivery, newItem: Delivery): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(delivery: Delivery?)
    }


}