package s4m.mobin.mobins4m.ui.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import io.reactivex.disposables.CompositeDisposable
import s4m.mobin.mobins4m.models.Delivery
import s4m.mobin.mobins4m.pagination.FeedDataSource
import s4m.mobin.mobins4m.pagination.FeedDataSourceFactory
import s4m.mobin.mobins4m.pagination.State

class LalaMoveImpl : ViewModel() {

    val deliveryList: LiveData<PagedList<Delivery>>
    private val compositeDisposable = CompositeDisposable()
    private val feedDataSourceFactory: FeedDataSourceFactory = FeedDataSourceFactory(compositeDisposable)

    init {
        val config = PagedList.Config.Builder()
                .setPageSize(10)
                .setInitialLoadSizeHint(10)
                .setEnablePlaceholders(false)
                .build()
        deliveryList = LivePagedListBuilder<Int, Delivery>(feedDataSourceFactory, config).build()
    }

    fun getState(): LiveData<State> = Transformations.switchMap(feedDataSourceFactory.feedDataSourceLiveData, FeedDataSource::state)

    fun retry() = feedDataSourceFactory.feedDataSourceLiveData.value?.retry()

    fun listIsEmpty() = deliveryList.value?.isEmpty() ?: true

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
        feedDataSourceFactory.feedDataSourceLiveData.value?.clear()
    }

}