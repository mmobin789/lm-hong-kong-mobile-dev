package s4m.mobin.mobins4m.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import s4m.mobin.mobins4m.R
import s4m.mobin.mobins4m.adapters.DeliveryAdapter
import s4m.mobin.mobins4m.adapters.DeliveryOfflineAdapter
import s4m.mobin.mobins4m.models.Delivery
import s4m.mobin.mobins4m.pagination.State
import s4m.mobin.mobins4m.storage.AppStorage
import s4m.mobin.mobins4m.ui.viewmodel.LalaMoveImpl


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }


    private fun init() {
        AppStorage.init(this)
        rv.layoutManager = LinearLayoutManager(this)
        if (isNetworkConnected())
            getDeliveries()
        else getDeliveriesCached()
    }


    private fun isNetworkConnected(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        return cm.activeNetworkInfo != null
    }

    private fun getDeliveriesCached() {
        loader.hide()
        val list = AppStorage.cacheDeliveries(null, true)
        if (list != null && list.isNotEmpty()) {
            errorTV.visibility = View.GONE
            rv.adapter = DeliveryOfflineAdapter(object : DeliveryOfflineAdapter.OnItemClickListener {
                override fun onItemClick(delivery: Delivery?) {
                    if (delivery != null) {
                        val detail = Intent(this@MainActivity, Detail::class.java)
                                .putExtra("delivery", delivery)
                        startActivity(detail)
                    }
                }
            }, list)
        } else {
            errorTV.setText(R.string.na)
            errorTV.visibility = View.VISIBLE
        }


    }

    private fun getDeliveries() {
        val lalaMoveImpl = ViewModelProviders.of(this).get(LalaMoveImpl::class.java)
        val adapter = DeliveryAdapter(object : DeliveryAdapter.OnItemClickListener {
            override fun onItemClick(delivery: Delivery?) {
                if (delivery != null) {
                    val detail = Intent(this@MainActivity, Detail::class.java)
                            .putExtra("delivery", delivery)
                    startActivity(detail)
                }
            }
        }) { lalaMoveImpl.retry() }

        rv.adapter = adapter
        lalaMoveImpl.deliveryList.observe(this, Observer {
            adapter.submitList(it)

        })


        errorTV.setOnClickListener { lalaMoveImpl.retry() }

        lalaMoveImpl.getState().observe(this, Observer { state ->
            loader.visibility = if (lalaMoveImpl.listIsEmpty() && state == State.LOADING) View.VISIBLE else View.GONE
            errorTV.visibility = if (lalaMoveImpl.listIsEmpty() && state == State.ERROR) {
                errorTV.setText(R.string.error_list)
                View.VISIBLE
            } else View.GONE
            if (!lalaMoveImpl.listIsEmpty()) {
                adapter.setState(state ?: State.DONE)
            }
        })
    }


}
