package s4m.mobin.mobins4m.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.adapter_ui.*
import s4m.mobin.mobins4m.GlideApp
import s4m.mobin.mobins4m.R
import s4m.mobin.mobins4m.models.Delivery

class Detail : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        init()

    }

    override fun onStart() {
        super.onStart()
        map.onStart()
    }

    private fun init() {
        val delivery = intent.getParcelableExtra<Delivery>("delivery")
        map.onCreate(null)
        map.getMapAsync {
            if (delivery.location != null) {
                it.addMarker(MarkerOptions().title(delivery.location.address).position(LatLng(delivery.location.lat, delivery.location.lng)))
                zoomToMyLocation(it, delivery.location.lat, delivery.location.lng)
            }
        }

        tv.text = if (delivery.description.isNullOrBlank())
            getString(R.string.no_desc)
        else delivery.description

        GlideApp.with(this).load(delivery.imageUrl).placeholder(R.drawable.ic_launcher_background).into(iv)

    }

    private fun zoomToMyLocation(googleMap: GoogleMap, lat: Double, lng: Double) {

        val latLng = LatLng(lat, lng)
        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 12f)
        googleMap.animateCamera(cameraUpdate)

    }

    override fun onPause() {
        super.onPause()
        map.onPause()

    }

    override fun onResume() {
        super.onResume()
        map.onResume()
    }

    override fun onStop() {
        super.onStop()
        map.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        map.onDestroy()
    }
}
