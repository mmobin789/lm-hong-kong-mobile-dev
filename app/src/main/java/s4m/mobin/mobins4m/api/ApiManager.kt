package s4m.mobin.mobins4m.api

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


/**
 * Created by MohammedMobinMunir on 4/17/2018.
 */
object ApiManager {


    private var retrofit: Retrofit? = null

    fun getAPI(): API {

        if (retrofit == null) {
            //val baseURL = "http://94.56.199.34/"
            val baseURL = "https://mock-api-mobile.dev.lalamove.com/"
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient().newBuilder().readTimeout(20, TimeUnit.SECONDS)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(20, TimeUnit.SECONDS).addInterceptor(logging).build()


            retrofit = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl(baseURL)
                    .client(client)
                    .build()
        }
        return retrofit!!.create(API::class.java)


    }


}