package s4m.mobin.mobins4m.api

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
import s4m.mobin.mobins4m.models.Delivery

/**
 * Created by MohammedMobinMunir on 4/17/2018.
 */
interface API {
    @GET("deliveries")
    fun getDeliveries(@Query("offset") offset: Int,
                      @Query("limit") limit: Int): Observable<MutableList<Delivery>>
}