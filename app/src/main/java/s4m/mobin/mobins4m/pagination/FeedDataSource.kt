package s4m.mobin.mobins4m.pagination

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.ItemKeyedDataSource
import android.util.Log
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers
import s4m.mobin.mobins4m.api.ApiManager
import s4m.mobin.mobins4m.models.Delivery
import s4m.mobin.mobins4m.storage.AppStorage

class FeedDataSource(private val compositeDisposable: CompositeDisposable) : ItemKeyedDataSource<Int, Delivery>() {
    private val offlineDeliveries = mutableListOf<Delivery>()
    override fun getKey(item: Delivery): Int {
        return offset
    }

    private var retryCompletable: Completable? = null
    val state = MutableLiveData<State>()
    private var offset = 0

    private fun updateCacheList(list: List<Delivery>, initialLoad: Boolean) {
        if (initialLoad)
            offlineDeliveries.clear()
        offlineDeliveries.addAll(list)
        AppStorage.cacheDeliveries(offlineDeliveries, false)
    }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Delivery>) {
        updateState(State.LOADING)
        Log.i("TotalItemsToLoadInitial", params.requestedLoadSize.toString())
        compositeDisposable.add(ApiManager.getAPI().getDeliveries(offset, params.requestedLoadSize).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe({ list ->
                    updateState(State.DONE)
                    offset += 10
                    updateCacheList(list, true)
                    callback.onResult(list!!)


                }, {
                    updateState(State.ERROR)
                    setRetry(Action { loadInitial(params, callback) })
                    it.printStackTrace()
                }))
    }


    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Delivery>) {
        updateState(State.LOADING)
        Log.i("ItemsToLoadAfter", params.key.toString())
        Log.i("TotalItemsToLoad", params.requestedLoadSize.toString())
        Log.i("offset", offset.toString())
        compositeDisposable.add(ApiManager.getAPI().getDeliveries(params.key, params.requestedLoadSize).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe({ list ->
                    updateState(State.DONE)
                    offset += 10
                    updateCacheList(list, false)
                    callback.onResult(list)


                }, {
                    updateState(State.ERROR)
                    setRetry(Action { loadAfter(params, callback) })
                    it.printStackTrace()

                }))
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Delivery>) {
    }

    fun retry() {
        if (retryCompletable != null) {
            compositeDisposable.add(retryCompletable!!
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe())
        }
    }

    private fun updateState(state: State) {
        this.state.postValue(state)
    }

    private fun setRetry(action: Action?) {
        retryCompletable = if (action == null) null else Completable.fromAction(action)
    }

    fun clear() {
        compositeDisposable.clear()

    }

}