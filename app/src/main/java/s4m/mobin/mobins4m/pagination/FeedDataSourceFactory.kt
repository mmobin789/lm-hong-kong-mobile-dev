package s4m.mobin.mobins4m.pagination

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import io.reactivex.disposables.CompositeDisposable
import s4m.mobin.mobins4m.models.Delivery

class FeedDataSourceFactory(private val compositeDisposable: CompositeDisposable) : DataSource.Factory<Int, Delivery>() {

    val feedDataSourceLiveData = MutableLiveData<FeedDataSource>()

    override fun create(): DataSource<Int, Delivery> {
        val feedDataSource = FeedDataSource(compositeDisposable)
        feedDataSourceLiveData.postValue(feedDataSource)
        return feedDataSource

    }
}