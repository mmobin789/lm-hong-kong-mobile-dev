package s4m.mobin.mobins4m.pagination

enum class State {
    DONE, LOADING, ERROR
}