package s4m.mobin.mobins4m.storage

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import s4m.mobin.mobins4m.models.Delivery

object AppStorage {

    private lateinit var sharedPreferences: SharedPreferences

    fun init(context: Context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    }


    fun cacheDeliveries(list: MutableList<Delivery>?, isFetch: Boolean): List<Delivery>? {
        val sh = sharedPreferences
        val gson = Gson()
        val listType = object : TypeToken<List<Delivery>>() {
        }.type

        if (isFetch) {
            return gson.fromJson(sh.getString("list", null), listType)
        } else {
            Log.i("CacheSize", list!!.size.toString())
            val json = gson.toJson(list, listType)
            sh.edit().putString("list", json).apply()
        }

        return null

    }
}