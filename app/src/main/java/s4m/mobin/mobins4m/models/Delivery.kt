package s4m.mobin.mobins4m.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Delivery
(
        val id: Int,
        val description: String?,
        val imageUrl: String?,
        val location: Location?
) : Parcelable {
    @Parcelize
    data class Location(val lat: Double, val lng: Double, val address: String) : Parcelable

}