package s4m.mobin.mobins4m.contracts

import s4m.mobin.mobins4m.models.Delivery

interface OnDeliveriesListener {
    fun onDeliveries(deliveries: List<Delivery?>?)

    fun onFailed(error: String)
}